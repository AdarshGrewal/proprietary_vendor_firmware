FIRMWARE_IMAGES := \
    aboot \
    apdp \
    bootlocker \
    cmnlib32 \
    cmnlib64 \
    devcfg \
    hosd \
    hyp \
    keymaster \
    modem \
    pmic \
    rpm \
    tz \
    xbl

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
